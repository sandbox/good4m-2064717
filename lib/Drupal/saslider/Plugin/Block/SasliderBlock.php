<?php
/* ⓢⓐⓣⓔⓒⓗⓐ.ⓒⓞⓜ
 * SaSlider by Satecha.com */

namespace Drupal\saslider\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an Instagram block.
 *
 * @Plugin(
 *   id = "saslider_block",
 *   admin_label = @Translation("Saslider block"),
 *   module = "saslider"
 * )
 */
class SasliderBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, array $plugin_definition) {
        return new static($configuration, $plugin_id, $plugin_definition, $container->get('request'));
    }

    /**
     * Overrides \Drupal\block\BlockBase::access().
     */
    public function access() {
        return TRUE;
    }
    
    /**
     * Gather all image filenames from the /banners/ directory in the module folder.
     * @return array of filenames
     */
    private function getImages() {
        $images = array();
        $pathToImages = drupal_get_path('module', 'saslider') . '/banners/*.*';
        foreach (glob($pathToImages) as $filename) {
            $images[] = $filename;
        }
        return $images;
    }

    /**
     * Build out array of form elements
     * @param array $form
     * @param array $form_state
     * @return type
     */
    public function buildConfigurationForm(array $form, array &$form_state) {
        $form = array();
        $configuration = $this->configuration;

        $form['width'] = array(
            '#type' => 'textfield',
            '#title' => t('Slider width in pixels:'),
            '#default_value' => isset($configuration['width']) ? $configuration['width'] : '400',
        );
        $form['height'] = array(
            '#type' => 'textfield',
            '#title' => t('Slider height in pixels:'),
            '#default_value' => isset($configuration['height']) ? $configuration['height'] : '200',
        );
        $form['transitionType'] = array(
            '#type' => 'select',
            '#title' => t('Effect used when transitioning between slides:'),
            '#options' => array (
               0 => t('Fade'),
               1 => t('Slide Left'), 
               2 => t('Slide Right'), 
            ),
            '#default_value' => isset($configuration['transitionType']) ? $configuration['transitionType'] : '0',
        );
        $form['duration'] = array(
            '#type' => 'textfield',
            '#title' => t('Duration between transitions in seconds:'),
            '#default_value' => isset($configuration['duration']) ? $configuration['duration'] : '5',
        );
        $form['transitionSpeed'] = array(
            '#type' => 'textfield',
            '#title' => t('Duration of transitions in seconds:'),
            '#default_value' => isset($configuration['transitionSpeed']) ? $configuration['transitionSpeed'] : '2',
        );
        
        $form['imagesInstructions'] = array(
            '#markup' => '<br><strong>Note:</strong> Saslider loads images from the /modules/saslider/banners/ directory.<br>You can control the order in which they appear by numbering or alphabetizing them sequentially.',
            '#weight' => 5
        );
        
        $form['imageDetails'] = array(
            '#type' => 'details',
            '#title' => t('Images'),
            '#weight' => 5.1,
            '#collapsible' => TRUE, 
            '#collapsed' => TRUE,
            '#suffix' => '<br>'
        );
        
        $images = $this->getImages();
        for($i = 0; $i < sizeof($images); $i++) {
            $filename = basename($images[$i]);
            $imagesString = '<label>Filename: ' . $filename . '</label>';
        
            $form['imageDetails']["filename$i"] = array(
                '#markup' => $imagesString,
            );
            $form['imageDetails']["label$i"] = array(
                '#type' => 'textfield',
                '#title' => t('Label text:'),
                '#default_value' => isset($configuration['imageDetails']["label$i"]) ? $configuration['imageDetails']["label$i"] : '',
            );
            $form['imageDetails']["link$i"] = array(
                '#type' => 'textfield',
                '#title' => t('Link URL:'),
                '#default_value' => isset($configuration['imageDetails']["link$i"]) ? $configuration['imageDetails']["link$i"] : '',
                '#suffix' => '<br><br>'
            );
        }
        

        return parent::buildConfigurationForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, array &$form_state) {
        if (!form_get_errors()) {
            $this->configuration['width'] = $form_state['values']['width'];
            $this->configuration['height'] = $form_state['values']['height'];
            $this->configuration['transitionType'] = $form_state['values']['transitionType'];
            $this->configuration['duration'] = $form_state['values']['duration'];
            $this->configuration['transitionSpeed'] = $form_state['values']['transitionSpeed'];
            for($i = 0; $i < 11; $i++) {
                if(isset($form_state['values']['imageDetails']["label$i"])) {
                    $this->configuration['imageDetails']["label$i"] = $form_state['values']['imageDetails']["label$i"];
                }
                if(isset($form_state['values']['imageDetails']["link$i"])) {
                    $this->configuration['imageDetails']["link$i"] = $form_state['values']['imageDetails']["link$i"];
                }
            }
            
            $details = array();
            for($i = 0; $i < 11; $i++) {
                if(isset($form_state['values']['imageDetails']["label$i"])) {
                    $details["label$i"] = $form_state['values']['imageDetails']["label$i"];
                }
                if(isset($form_state['values']['imageDetails']["link$i"])) {
                    $details["link$i"] = $form_state['values']['imageDetails']["link$i"];
                }
            }
            $this->configuration['imageDetailsArray'] = $details;
        }
        parent::submitConfigurationForm($form, $form_state);
    }
    
    

    /**
     * {@inheritdoc}
     */
    public function build() {
        $configuration = $this->configuration;
        drupal_add_js('http://d3lp1msu2r81bx.cloudfront.net/kjs/js/lib/kinetic-v4.5.5.min.js', 'external');

        $content['children']['saslider'] = array(
            '#markup' => '',
            '#theme' => 'saslider_block_image',
            '#images' => $this->getImages(),
            '#width' => $configuration['width'],
            '#height' => $configuration['height'],
            '#transitionType' => $configuration['transitionType'],
            '#duration' => $configuration['duration'],
            '#transitionSpeed' => $configuration['transitionSpeed'],
            '#imageDetailsArray' => $configuration['imageDetailsArray'],
        );

        $block['content'] = $content;

        return $block;
    }

}